json.array!(@tasks) do |task|
  json.extract! task, :id, :title, :text, :time, :priority
  json.url task_url(task, format: :json)
end
