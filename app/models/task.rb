class Task < ActiveRecord::Base
 validates :title, presence: true
 validates :priority, inclusion: { in: %w(low medium HIGH), message: "wrong %{value}" }


def self.search(search,type)
if search
    case type
        when "done?"
                where('completed IS NULL')
        when "title"
                where('title LIKE ?', "%#{search}%")
        when "text tag"
                where('text LIKE ?', "%#{search}%")
        when "till date"                                
                where('time LIKE ?', "%#{search}%")
        when "priority"
                where('priority LIKE ?', "%#{search}%")
    end
else
    scoped
end        

end
end
