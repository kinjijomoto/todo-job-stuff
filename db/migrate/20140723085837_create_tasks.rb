class CreateTasks < ActiveRecord::Migration
  def change
    create_table :tasks do |t|
      t.string :title
      t.text :text
      t.date :time
      t.string :priority

      t.timestamps
    end
  end
end
