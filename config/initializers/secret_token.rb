# Be sure to restart your server when you modify this file.

# Your secret key is used for verifying the integrity of signed cookies.
# If you change this key, all old signed cookies will become invalid!

# Make sure the secret is at least 30 characters and all random,
# no regular words or you'll be exposed to dictionary attacks.
# You can use `rake secret` to generate a secure secret key.

# Make sure your secret_key_base is kept private
# if you're sharing your code publicly.
Bitodo::Application.config.secret_key_base = 'ba87b373dc2d0be6557dc99ec1c0ea961719c79fd862c349fce4831288f6d0ef4bb37d6902e6379bd5c2ec5a33f114bc109357fad5c0fdb91fd17dc742fb333b'
